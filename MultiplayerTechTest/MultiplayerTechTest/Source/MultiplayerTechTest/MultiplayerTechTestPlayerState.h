// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "./Spawn/SpawnZone.h"
#include "MultiplayerTechTestCharacter.h"
#include "MultiplayerTechTestPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTECHTEST_API AMultiplayerTechTestPlayerState : public APlayerState
{
	GENERATED_BODY()

	FTimerHandle RespawnTimerHandle;

	bool bTimerStarted = false;

	float TimeToFinishTimer = 0.f;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	float CurrentRespawnTime = 0.f;

	ASpawnZone * SpawnZoneRef;

	UFUNCTION(BlueprintInternalUseOnly)
	void PrepareRespawnCharacter(ASpawnZone * SpawnZone);

public:
	AMultiplayerTechTestPlayerState();

	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Server, unreliable, WithValidation)
	void Server_RespawnTimerStart(float TimeToRespawn, ASpawnZone * SpawnZone);


	UFUNCTION(Server, unreliable, WithValidation)
	void Server_RespawnCharacter(AMultiplayerTechTestCharacter * DeadCharacter, FVector NewPosition);
	
};
