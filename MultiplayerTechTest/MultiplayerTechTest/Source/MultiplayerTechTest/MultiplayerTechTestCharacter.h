// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Net/UnrealNetwork.h"
#include "Interfaces/MultiplayerCharacterInterface.h"
#include "MultiplayerTechTestCharacter.generated.h"

class UInputComponent;
class UHealthComponent;

UCLASS(config=Game)
class AMultiplayerTechTestCharacter : public ACharacter, public IMultiplayerCharacterInterface
{
	GENERATED_BODY()
		
	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Pawn mesh: Multiplayer person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* Mesh_MP;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_Gun;	

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Health Component. */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UHealthComponent* HealthComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* L_MotionController;

	FTimerHandle RespawnTimerHandle;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* SpecialAttack;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "SpecialAttack", meta = (AllowPrivateAccess = "true"))
	bool bSpecialAttackIsGrowing = false;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "SpecialAttack", meta = (AllowPrivateAccess = "true"))
	bool bSpecialAttackInCooldown = false;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "SpecialAttack", meta = (AllowPrivateAccess = "true"))
	float SpecialAttackNextActivationTime;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "SpecialAttack", meta = (AllowPrivateAccess = "true"))
	float SpecialAttackCharge = 0.f;

	UPROPERTY(Replicated, VisibleDefaultsOnly, BlueprintReadOnly, Category = "SpecialAttack", meta = (AllowPrivateAccess = "true"))
	float SpecialAttackCurrentCooldDownTime= 0.f;
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SpecialAttack")
	float SpecialAttackMaxRange = 400.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SpecialAttack")
	float SpecialAttackGrowPerSecond = 133.33f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SpecialAttack")
	float SpecialAttackCooldownTime = 3.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SpecialAttack")
		float SpecialAttackDamage = 20.f;

	AMultiplayerTechTestCharacter();

protected:
	virtual void BeginPlay();
	
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(Server, unreliable, WithValidation)
	void Server_Fire();

	UFUNCTION(Server, unreliable, WithValidation)
	void Server_SpecialAttackStart();

	UFUNCTION(Server, unreliable, WithValidation)
	void Server_SpecialAttackStop();

	UFUNCTION(NetMulticast, unreliable, WithValidation)
	void Multicast_Fire();

	UFUNCTION(NetMulticast, unreliable, WithValidation)
	void Multicast_Die();

	UFUNCTION(NetMulticast, unreliable, WithValidation)
	void Multicast_DrawSpecialAttackDebugSphere(FVector Center, float Radius);

	UFUNCTION(Client, unreliable)
	void Client_UpdateSpecialAttackTimerVisibility(bool NewVisibility);

	UFUNCTION(Client, unreliable)
	void Client_UpdateRespawnVisibility(bool NewVisibility);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void HandleDestroy(); void HandleDestroy_Implementation();

public:

	UFUNCTION(NetMulticast, unreliable, WithValidation)
		void Multicast_Respawn(FVector NewPosition);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void SetSpecialAttackTimerVisibility(bool NewVisibility); void SetSpecialAttackTimerVisibility_Implementation(bool NewVisibility);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void SetRespawnVisibility(bool NewVisibility); void SetRespawnVisibility_Implementation(bool NewVisibility);

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AMultiplayerTechTestProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint32 bUsingMotionControllers : 1;

protected:
	
	/** Fires a projectile. */
	void OnFire();

	/** Starts special attack. */
	void OnSpecialPressed();

	/** Stops special attack. */
	void OnSpecialReleased();

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

};

