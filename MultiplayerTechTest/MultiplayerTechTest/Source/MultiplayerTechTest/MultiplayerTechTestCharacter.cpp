// Copyright Epic Games, Inc. All Rights Reserved.

#include "MultiplayerTechTestCharacter.h"
#include "MultiplayerTechTestProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Components/InputComponent.h"
#include "./Components/HealthComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AMultiplayerTechTestCharacter

AMultiplayerTechTestCharacter::AMultiplayerTechTestCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
	GetCapsuleComponent()->SetIsReplicated(true);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	//Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	Mesh_MP = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CharacterMeshMP"));
	Mesh_MP->SetOnlyOwnerSee(false);
	Mesh_MP->SetOwnerNoSee(true);
	Mesh_MP->SetupAttachment(FirstPersonCameraComponent);
	Mesh_MP->bCastDynamicShadow = false;
	Mesh_MP->CastShadow = false;
	Mesh_MP->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
	Mesh_MP->SetRelativeLocation(FVector(8.944336, -7.687018, -155.7f));
	
	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->SetIsReplicated(true);


	// Initializes SpecialAttack Sphere Collider
	SpecialAttack = CreateDefaultSubobject<USphereComponent>(TEXT("SpecialAttack"));
	SpecialAttack->BodyInstance.SetCollisionProfileName("OverlapAllDynamic");
	SpecialAttack->SetupAttachment(RootComponent);
	SpecialAttack->SetIsReplicated(true);
	SpecialAttack->InitSphereRadius(0.f);
}


void AMultiplayerTechTestCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	// Here we list the variables we want to replicate + a condition if wanted
	DOREPLIFETIME(AMultiplayerTechTestCharacter, SpecialAttackCharge);
	DOREPLIFETIME(AMultiplayerTechTestCharacter, SpecialAttackCurrentCooldDownTime);
}

void AMultiplayerTechTestCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("AMultiplayerTechTestCharacter::BeginPlay [LocalRole:%s]: Enter "), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")));

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

	if (GetLocalRole() >= ROLE_Authority) {
		HealthComponent->InitializeHealth(100.f);
		UE_LOG(LogTemp, Warning, TEXT("AMultiplayerTechTestCharacter::BeginPlay [LocalRole:%s]: HealthInitlalzed "), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")));
	}
}

void AMultiplayerTechTestCharacter::Tick(float DeltaTime)  {
	Super::Tick(DeltaTime);

	if (GetLocalRole() >= ROLE_Authority) {
		// Checks if Special Attack is being casted and makes the Special Attack Sphere bigger
		if (bSpecialAttackIsGrowing && !bSpecialAttackInCooldown) {

			float Radius = SpecialAttack->GetUnscaledSphereRadius();

			if (Radius < SpecialAttackMaxRange) {
				Radius = FMath::Clamp(Radius + DeltaTime * SpecialAttackGrowPerSecond, 0.f, SpecialAttackMaxRange);
				SpecialAttack->SetSphereRadius(Radius);
			}

			SpecialAttackCharge = FMath::Clamp(Radius / SpecialAttackMaxRange, 0.f, 1.f);

			Multicast_DrawSpecialAttackDebugSphere(SpecialAttack->GetComponentLocation(), Radius);
		}

		// Checks if Special Attack is in cooldown time
		if (bSpecialAttackInCooldown) {
			UE_LOG(LogTemp, Warning, TEXT("AMultiplayerTechTestCharacter::Tick Restoring %f till %f"), SpecialAttackNextActivationTime, GetWorld()->GetTimeSeconds());
			
			SpecialAttackCharge = 0.f;
			SpecialAttackCurrentCooldDownTime = FMath::Clamp(SpecialAttackNextActivationTime - GetWorld()->GetTimeSeconds(), 0.f, SpecialAttackCooldownTime);

			// If Special Attack cooldown time is over, renables it
			if (SpecialAttackNextActivationTime <= GetWorld()->GetTimeSeconds()) {
				bSpecialAttackInCooldown = false;
				Client_UpdateSpecialAttackTimerVisibility(false);
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AMultiplayerTechTestCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMultiplayerTechTestCharacter::OnFire);

	// Bind special event
	PlayerInputComponent->BindAction("Special", IE_Pressed, this, &AMultiplayerTechTestCharacter::OnSpecialPressed);
	PlayerInputComponent->BindAction("Special", IE_Released, this, &AMultiplayerTechTestCharacter::OnSpecialReleased);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AMultiplayerTechTestCharacter::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AMultiplayerTechTestCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMultiplayerTechTestCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMultiplayerTechTestCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMultiplayerTechTestCharacter::LookUpAtRate);
}

void AMultiplayerTechTestCharacter::OnFire()
{
	if (GetLocalRole() < ROLE_Authority) {
		Server_Fire();
	}
}
bool AMultiplayerTechTestCharacter::Server_Fire_Validate() {
	return true;
}

void AMultiplayerTechTestCharacter::Server_Fire_Implementation() {

	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			AMultiplayerTechTestProjectile * Bullet;

			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				Bullet = World->SpawnActor<AMultiplayerTechTestProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				Bullet = World->SpawnActor<AMultiplayerTechTestProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}

			if (Bullet) {
				Bullet->SetOwner(GetController());
			}
		}
	}

	// Call every client replicate sounds and animations related with fire a gun event
	Multicast_Fire();
}

bool AMultiplayerTechTestCharacter::Multicast_Fire_Validate() {
	return true;
}

void AMultiplayerTechTestCharacter::Multicast_Fire_Implementation() {
	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AMultiplayerTechTestCharacter::OnSpecialPressed()
{
	if (GetLocalRole() < ROLE_Authority) {
		UE_LOG(LogTemp, Warning, TEXT("AMultiplayerTechTestCharacter::OnSpecialPressed"));
		Server_SpecialAttackStart();
	}
}

void AMultiplayerTechTestCharacter::OnSpecialReleased()
{
	if (GetLocalRole() < ROLE_Authority) {
		UE_LOG(LogTemp, Warning, TEXT("AMultiplayerTechTestCharacter::OnSpecialReleased"));
		Server_SpecialAttackStop();
	}
}

bool AMultiplayerTechTestCharacter::Server_SpecialAttackStart_Validate() {
	return true;
}

void AMultiplayerTechTestCharacter::Server_SpecialAttackStart_Implementation() {
	bSpecialAttackIsGrowing = true;
	UE_LOG(LogTemp, Warning, TEXT("AMultiplayerTechTestCharacter::Server_SpecialAttackStart_Implementation"));
}

bool AMultiplayerTechTestCharacter::Server_SpecialAttackStop_Validate() {
	return true;
}

void AMultiplayerTechTestCharacter::Server_SpecialAttackStop_Implementation() {
	bSpecialAttackIsGrowing = false;

	// Check is a valid Special Attack release, meaning attack has properly started and it is not in cooldown time
	if (!bSpecialAttackInCooldown) {
		TArray<AActor *> OverlappingActors;
		SpecialAttack->GetOverlappingActors(OverlappingActors, nullptr);


		// Search for every actor inside the sphere different than the caster, and ApplyDamage on it
		for (auto Actor : OverlappingActors) {
			UE_LOG(LogTemp, Warning, TEXT("AMultiplayerTechTestCharacter::Server_SpecialAttackStop_Implementation I'm [%s] Found %s "), * GetName(), * Actor->GetName());

			if (Actor != this) {
				UE_LOG(LogTemp, Warning, TEXT("AMultiplayerTechTestCharacter::Server_SpecialAttackStop_Implementation I'm [%s] Damaged %s with %f"), * GetName(), * Actor->GetName(), SpecialAttackDamage);
				UGameplayStatics::ApplyDamage(Actor, SpecialAttackDamage, GetInstigatorController(), this, nullptr);
			}
		}

		// Activate Special Attack Cooldown and Updates UI
		SpecialAttack->SetSphereRadius(0.f);
		bSpecialAttackInCooldown = true;
		SpecialAttackNextActivationTime = GetWorld()->GetTimeSeconds() + SpecialAttackCooldownTime;
		Client_UpdateSpecialAttackTimerVisibility(true);
	}
}

void AMultiplayerTechTestCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AMultiplayerTechTestCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AMultiplayerTechTestCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

void AMultiplayerTechTestCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AMultiplayerTechTestCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AMultiplayerTechTestCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMultiplayerTechTestCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AMultiplayerTechTestCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMultiplayerTechTestCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AMultiplayerTechTestCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AMultiplayerTechTestCharacter::TouchUpdate);
		return true;
	}
	
	return false;
}

//////////////////////////////////////////////////////////////////////////
// Die and respawn

void AMultiplayerTechTestCharacter::HandleDestroy_Implementation() {
	Multicast_Die();
	Client_UpdateRespawnVisibility(true);
}


bool AMultiplayerTechTestCharacter::Multicast_Die_Validate() {
	return true;
}

void AMultiplayerTechTestCharacter::Multicast_Die_Implementation() {
	//Disables input after death
	if (IsLocallyControlled()) {
		APlayerController * PlayerController = Cast<APlayerController>(GetController());
		if (Cast<APlayerController>(GetController())) {
			DisableInput(PlayerController);
		}
	}

	//Hide and disable character
	SetActorHiddenInGame(true);
	GetCapsuleComponent()->SetEnableGravity(false);
	GetCharacterMovement()->GravityScale = 0.f;
	SetActorEnableCollision(false);
}

bool AMultiplayerTechTestCharacter::Multicast_Respawn_Validate(FVector NewPosition) {
	return true;
}

void AMultiplayerTechTestCharacter::Multicast_Respawn_Implementation(FVector NewPosition) {
	// Reinitialize health and relocate the character
	if (GetLocalRole() >= ROLE_Authority) {
		HealthComponent->InitializeHealth(100.f);
		SetActorLocation(NewPosition);

		Client_UpdateRespawnVisibility(false);
	}

	// Show and enable character
	SetActorEnableCollision(true);
	GetCapsuleComponent()->SetEnableGravity(true);
	GetCharacterMovement()->GravityScale = 1.f;
	SetActorHiddenInGame(false);

	// Enable character input
	if (IsLocallyControlled()) {
		APlayerController * PlayerController = Cast<APlayerController>(GetController());
		if (Cast<APlayerController>(GetController())) {
			EnableInput(PlayerController);
		}
	}
}


//////////////////////////////////////////////////////////////////////////
// UI
void AMultiplayerTechTestCharacter::Client_UpdateSpecialAttackTimerVisibility_Implementation(bool NewVisibility) {
	SetSpecialAttackTimerVisibility(NewVisibility);
}

void AMultiplayerTechTestCharacter::Client_UpdateRespawnVisibility_Implementation(bool NewVisibility) {
	SetRespawnVisibility(NewVisibility);
}

void AMultiplayerTechTestCharacter::SetSpecialAttackTimerVisibility_Implementation(bool NewVisibility) {

}

void AMultiplayerTechTestCharacter::SetRespawnVisibility_Implementation(bool NewVisibility) {

}

//////////////////////////////////////////////////////////////////////////
// Debugging
bool AMultiplayerTechTestCharacter::Multicast_DrawSpecialAttackDebugSphere_Validate(FVector Center, float Radius) {
	return true;
}

void AMultiplayerTechTestCharacter::Multicast_DrawSpecialAttackDebugSphere_Implementation(FVector Center, float Radius) {

	DrawDebugSphere(
		GetWorld(),
		Center,
		Radius,
		12,
		IsLocallyControlled() ? FColor::Purple : FColor::Orange,
		false,
		0.f,
		0,
		3.f
	);
}