// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MultiplayerTechTestHUD.generated.h"

UCLASS()
class AMultiplayerTechTestHUD : public AHUD
{
	GENERATED_BODY()

public:
	AMultiplayerTechTestHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

