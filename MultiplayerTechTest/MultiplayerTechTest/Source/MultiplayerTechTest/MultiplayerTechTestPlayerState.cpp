// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerTechTestPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

AMultiplayerTechTestPlayerState::AMultiplayerTechTestPlayerState() {
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}

void AMultiplayerTechTestPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	// Here we list the variables we want to replicate + a condition if wanted
	DOREPLIFETIME(AMultiplayerTechTestPlayerState, CurrentRespawnTime);
}

void AMultiplayerTechTestPlayerState::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	if (GetLocalRole() >= ROLE_Authority) {
		if (bTimerStarted) {
			CurrentRespawnTime = FMath::Clamp(TimeToFinishTimer - GetWorld()->GetTimeSeconds(), 0.f, CurrentRespawnTime);

			GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, FString::Printf(TEXT("AMultiplayerTechTestGameMode::Tick [LocalRole:%s]: %f"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), CurrentRespawnTime));


			if (TimeToFinishTimer <= GetWorld()->GetTimeSeconds()) {
				GEngine->AddOnScreenDebugMessage(1, 2, FColor::Red, FString::Printf(TEXT("AMultiplayerTechTestGameMode::Tick [LocalRole:%s]: Finish"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), CurrentRespawnTime));
				bTimerStarted = false;
				PrepareRespawnCharacter(SpawnZoneRef);
				PrimaryActorTick.bCanEverTick = false;
			}
		}
	}
}


bool AMultiplayerTechTestPlayerState::Server_RespawnTimerStart_Validate(float TimeToRespawn, ASpawnZone * SpawnZone) {
	return true;
}

void AMultiplayerTechTestPlayerState::Server_RespawnTimerStart_Implementation(float TimeToRespawn, ASpawnZone * SpawnZone) {
	PrimaryActorTick.bCanEverTick = true;
	TimeToFinishTimer = TimeToRespawn + GetWorld()->GetTimeSeconds();
	bTimerStarted = true;
	SpawnZoneRef = SpawnZone;
	CurrentRespawnTime = TimeToRespawn;
}

bool AMultiplayerTechTestPlayerState::Server_RespawnCharacter_Validate(AMultiplayerTechTestCharacter * DeadCharacter, FVector NewPosition) {
	return true;
}

void AMultiplayerTechTestPlayerState::Server_RespawnCharacter_Implementation(AMultiplayerTechTestCharacter * DeadCharacter, FVector NewPosition) {
	DeadCharacter->Multicast_Respawn(NewPosition);
}

void AMultiplayerTechTestPlayerState::PrepareRespawnCharacter(ASpawnZone * SpawnZone) {
	if (GetLocalRole() >= ROLE_Authority) {
		GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, FString::Printf(TEXT("AMultiplayerTechTestGameMode::RespawnDelegate [LocalRole:%s]: TimerFinished"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth"))));
		AMultiplayerTechTestCharacter * DeadCharacter = Cast<AMultiplayerTechTestCharacter>(GetPawn());

		if (DeadCharacter) {
			GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, FString::Printf(TEXT("AMultiplayerTechTestGameMode::RespawnDelegate [LocalRole:%s]: Respawn Char"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth"))));
			SpawnZoneRef->Server_GetRandomSpawnPosition(DeadCharacter);
		}
	}
}
