// Copyright Epic Games, Inc. All Rights Reserved.

#include "MultiplayerTechTestGameMode.h"
#include "MultiplayerTechTestHUD.h"
#include "MultiplayerTechTestCharacter.h"
#include "MultiplayerTechTestPlayerState.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Interfaces/MultiplayerCharacterInterface.h"

AMultiplayerTechTestGameMode::AMultiplayerTechTestGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMultiplayerTechTestHUD::StaticClass();

	// use our custom HUD class
	PlayerStateClass = AMultiplayerTechTestPlayerState::StaticClass();
}
void AMultiplayerTechTestGameMode::BeginPlay() {
	Super::BeginPlay();

	// Retrieve first SpawnZone for respawning
	TArray<AActor*> SpawnZonesFound;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASpawnZone::StaticClass(), SpawnZonesFound);

	if (SpawnZonesFound.Num() > 0) {
		SpawnZoneRef = Cast<ASpawnZone>(SpawnZonesFound[0]);
	}

}

void AMultiplayerTechTestGameMode::ActorDied(AActor * DeadActor) {
	if (IMultiplayerCharacterInterface* MultiplayerCharacterInterface = Cast<IMultiplayerCharacterInterface>(DeadActor)) {
		GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, 
			FString::Printf(TEXT("AMultiplayerTechTestGameMode::ActorDied [LocalRole:%s]: Actor %s"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), *DeadActor->GetName()));

		// Call to handle destroy interface
		MultiplayerCharacterInterface->Execute_HandleDestroy(DeadActor);
		AMultiplayerTechTestCharacter * Character = Cast<AMultiplayerTechTestCharacter>(DeadActor);

		// Checks that DeadActor is a valid character and launche RespawnTimer
		if (Character) {
			AMultiplayerTechTestPlayerState * PlayerState = Cast<AMultiplayerTechTestPlayerState>(Character->GetPlayerState());
			if (PlayerState) {
				GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, FString::Printf(TEXT("AMultiplayerTechTestGameMode::ActorDied [LocalRole:%s]: TimerStarted %f"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), TimeToRespawn));
				
				PlayerState->Server_RespawnTimerStart(TimeToRespawn, SpawnZoneRef);
			}
		}
	}
}


float AMultiplayerTechTestGameMode::GetTimeToRespawn() {
	return TimeToRespawn;
}