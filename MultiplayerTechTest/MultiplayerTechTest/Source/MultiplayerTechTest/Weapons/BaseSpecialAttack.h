// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "BaseSpecialAttack.generated.h"

UCLASS(Abstract)
class MULTIPLAYERTECHTEST_API ABaseSpecialAttack : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseSpecialAttack();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(Server, unreliable, WithValidation, BlueprintCallable)
	virtual void Server_SpecialAtackStart();

	UFUNCTION(Server, unreliable, WithValidation, BlueprintCallable)
	virtual void Server_SpecialAtackStop();
};
