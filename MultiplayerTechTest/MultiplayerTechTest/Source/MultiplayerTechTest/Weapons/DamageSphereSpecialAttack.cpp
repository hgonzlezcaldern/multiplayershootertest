// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageSphereSpecialAttack.h"
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "../MultiplayerTechTestCharacter.h"
#include "Components/SphereComponent.h"

ADamageSphereSpecialAttack::ADamageSphereSpecialAttack()
{
	bReplicates = true;

	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(0.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Trigger");
}

// Called every frame
void ADamageSphereSpecialAttack::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetLocalRole() >= ROLE_Authority) {
		if (bIsGrowing) {
			float Radius = CollisionComp->GetUnscaledSphereRadius();

			if (Radius < MaxSpecialRange) {
				Radius = FMath::Clamp(Radius + DeltaTime * SpecialGrowPerSecond, 0.f, MaxSpecialRange);
				CollisionComp->SetSphereRadius(Radius);
			}
			Multicast_DrawSpecialDebugSphere();
		}
	}
}


bool ADamageSphereSpecialAttack::Server_SpecialAtackStart_Validate() {
	return true;
}

void ADamageSphereSpecialAttack::Server_SpecialAtackStart_Implementation() {
	bIsGrowing = true;
}

bool ADamageSphereSpecialAttack::Server_SpecialAtackStop_Validate() {
	return true;
}

void ADamageSphereSpecialAttack::Server_SpecialAtackStop_Implementation() {
	bIsGrowing = false;
	CollisionComp->SetSphereRadius(0.f);
}

bool ADamageSphereSpecialAttack::Multicast_DrawSpecialDebugSphere_Validate() {
	return true;
}

void ADamageSphereSpecialAttack::Multicast_DrawSpecialDebugSphere_Implementation() {
	AMultiplayerTechTestCharacter * MultiplayerTechTestCharacter = Cast<AMultiplayerTechTestCharacter>(GetOwner());

	if (MultiplayerTechTestCharacter) {
		DrawDebugSphere(
			GetWorld(),
			GetActorLocation(),
			CollisionComp->GetScaledSphereRadius(),
			12,
			(MultiplayerTechTestCharacter->IsLocallyControlled()?FColor::Purple : FColor::Orange),
			false,
			10.f,
			0,
			30.f
		);
	}
}