// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseSpecialAttack.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ABaseSpecialAttack::ABaseSpecialAttack()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABaseSpecialAttack::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseSpecialAttack::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


bool ABaseSpecialAttack::Server_SpecialAtackStart_Validate() {
	return true;
}

void ABaseSpecialAttack::Server_SpecialAtackStart_Implementation() {
}

bool ABaseSpecialAttack::Server_SpecialAtackStop_Validate() {
	return true;
}

void ABaseSpecialAttack::Server_SpecialAtackStop_Implementation() {

}
