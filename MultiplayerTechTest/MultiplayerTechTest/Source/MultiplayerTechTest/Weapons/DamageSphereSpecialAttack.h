// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseSpecialAttack.h"
#include "DamageSphereSpecialAttack.generated.h"

class USphereComponent;

/**
 * 
 */
UCLASS()
class MULTIPLAYERTECHTEST_API ADamageSphereSpecialAttack : public ABaseSpecialAttack
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category = "Damage")
	class USphereComponent* CollisionComp;

	float MaxSpecialRange = 400.f;
	
	float SpecialGrowPerSecond = 100.f;

	float WaitTime = 4.f;

	bool bIsGrowing = false;

public:
	ADamageSphereSpecialAttack();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual bool Server_SpecialAtackStart_Validate() override;
	
	virtual void Server_SpecialAtackStart_Implementation() override;

	virtual bool Server_SpecialAtackStop_Validate() override;

	virtual void Server_SpecialAtackStop_Implementation() override;

	UFUNCTION(NetMulticast, unreliable, WithValidation, BlueprintCallable)
	void Multicast_DrawSpecialDebugSphere() ;
};
