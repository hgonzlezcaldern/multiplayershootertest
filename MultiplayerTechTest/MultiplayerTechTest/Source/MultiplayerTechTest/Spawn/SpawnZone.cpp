// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnZone.h"
#include "../MultiplayerTechTestPlayerState.h"
#include "Net/UnrealNetwork.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"

// Sets default values
ASpawnZone::ASpawnZone()
{
	bReplicates = true;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	CollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	CollisionComp->InitBoxExtent(FVector(10.f, 10.f, 10.f));
	CollisionComp->BodyInstance.SetCollisionProfileName("Trigger");
	RootComponent = CollisionComp;
}

// Called when the game starts or when spawned
void ASpawnZone::BeginPlay()
{
	Super::BeginPlay();
	
}

bool ASpawnZone::Server_GetRandomSpawnPosition_Validate(AMultiplayerTechTestCharacter * Character) {
	return true;
}

void ASpawnZone::Server_GetRandomSpawnPosition_Implementation(AMultiplayerTechTestCharacter * Character) {
	if (GetLocalRole() >= ROLE_Authority) {
		FVector NewPosition;

		// Find a new valid position
		bool CanSpawn = GetRandomPositionInsideBox(Character, NewPosition);

		while (!CanSpawn) {
			CanSpawn = GetRandomPositionInsideBox(Character, NewPosition);
		}

		// Prepare character to be respawn into a new location
		AMultiplayerTechTestPlayerState * PlayerState = Cast<AMultiplayerTechTestPlayerState>(Character->GetPlayerState());
		if (PlayerState) {
				PlayerState->Server_RespawnCharacter(Character, NewPosition);
		}
		
	}
}

bool ASpawnZone::GetRandomPositionInsideBox(AMultiplayerTechTestCharacter * Character, FVector& NewPosition) {
	if (GetLocalRole() >= ROLE_Authority) {
		Multicast_DrawSpawnZone();

		// Get random position inside spawn bounds
		NewPosition = UKismetMathLibrary::RandomPointInBoundingBox(CollisionComp->GetComponentLocation(), CollisionComp->GetScaledBoxExtent());
		// Set a fixed standard Z value for the position to assure character won't faall from the sky nor be stucked on the ground
		NewPosition.Z = GroundOffsetZ;

		UCapsuleComponent* CharCapsule = Character->GetCapsuleComponent();
		TArray<TEnumAsByte<EObjectTypeQuery>> SearchedObjectTypes;
		SearchedObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_WorldStatic));
		SearchedObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_WorldDynamic));
		SearchedObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_Pawn));
		SearchedObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_PhysicsBody));
		SearchedObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_Vehicle));
		SearchedObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_Destructible));

		TArray<AActor*> ActorsToIgnore;
		ActorsToIgnore.Add(this);
		ActorsToIgnore.Add(Character);

		TArray<AActor*> ActorOverlapped;

		// Check if given location is free to respawn by casting a Capsule the same size as the Character to be respawn
		bool CanSpawn = ! UKismetSystemLibrary::CapsuleOverlapActors(
			GetWorld(),
			NewPosition,
			CharCapsule->GetScaledCapsuleRadius(),
			CharCapsule->GetScaledCapsuleHalfHeight(),
			SearchedObjectTypes,
			nullptr,
			ActorsToIgnore,
			ActorOverlapped
		);

		Multicast_DrawDebugCapsule(
			NewPosition,
			CharCapsule->GetScaledCapsuleRadius(),
			CharCapsule->GetScaledCapsuleHalfHeight(),
			CanSpawn
		);
		
		Multicast_DrawDebugPosition(NewPosition, CanSpawn);

		return CanSpawn;
	}
	else {
		return false;
	}
}


//////////////////////////////////////////////////////////////////////////
// Debugging

bool ASpawnZone::Multicast_DrawSpawnZone_Validate() {
	return true;
}

void ASpawnZone::Multicast_DrawSpawnZone_Implementation() {
	DrawDebugBox(GetWorld(), GetActorLocation(), CollisionComp->GetScaledBoxExtent(),FColor::Purple, false, 10.f, 0, 30.f);
	GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, FString::Printf(TEXT("ASpawnZone::Multicast_DrawSpawnZone_Implementation [LocalRole:%s]: Location  %s; Extent %s"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), * GetActorLocation().ToString(), * CollisionComp->GetScaledBoxExtent().ToString()));
	UE_LOG(LogTemp, Warning, TEXT("ASpawnZone::Multicast_DrawSpawnZone_Implementation [LocalRole:%s]: Location  %s; Extent %s"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), *GetActorLocation().ToString(), *CollisionComp->GetScaledBoxExtent().ToString());
}

bool ASpawnZone::Multicast_DrawDebugPosition_Validate(FVector Center, bool Success) {
	return true;
}

void ASpawnZone::Multicast_DrawDebugPosition_Implementation(FVector Center, bool Success) {
	DrawDebugBox(GetWorld(), Center, FVector(10.f, 10.f, 10000.f), (Success ? FColor::Emerald : FColor::Red), false, 10.f, 0, 10.f);
	GEngine->AddOnScreenDebugMessage(1, 2, (Success?FColor::Emerald: FColor::Red), FString::Printf(TEXT("ASpawnZone::Multicast_DrawDebugPosition_Implementation [LocalRole:%s]: Location  %s; Extent %s"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), * Center.ToString(), * FVector(10.f, 10.f, 10000.f).ToString()));
	UE_LOG(LogTemp, Warning, TEXT("ASpawnZone::Multicast_DrawDebugPosition_Implementation [LocalRole:%s]: Location  %s; Extent %s"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), *Center.ToString(), *FVector(10.f, 10.f, 10000.f).ToString());
}

bool ASpawnZone::Multicast_DrawDebugCapsule_Validate(FVector Center, float Radius, float HalfHeight, bool Success) {
	return true;
}

void ASpawnZone::Multicast_DrawDebugCapsule_Implementation(FVector Center, float Radius, float HalfHeight, bool Success) {
	DrawDebugCapsule(GetWorld(), Center, HalfHeight, Radius, FQuat(FRotator(0.f)), FColor::Cyan, false, 10.f, 0, 3.f);
	GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, FString::Printf(TEXT("ASpawnZone::Multicast_DrawDebugCapsule_Implementation [LocalRole:%s]: Location  %s; Radius %f; ; HalfHeight %f"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), * Center.ToString(), Radius, HalfHeight));
	UE_LOG(LogTemp, Warning, TEXT("ASpawnZone::Multicast_DrawDebugCapsule_Implementation [LocalRole:%s]: Location  %s; Radius %f; ; HalfHeight %f"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), *Center.ToString(), Radius, HalfHeight);
}
