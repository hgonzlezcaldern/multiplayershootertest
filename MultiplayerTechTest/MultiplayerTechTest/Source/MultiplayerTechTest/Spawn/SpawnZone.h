// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../MultiplayerTechTestCharacter.h"
#include "SpawnZone.generated.h"

class UBoxComponent;

UCLASS()
class MULTIPLAYERTECHTEST_API ASpawnZone : public AActor
{
	GENERATED_BODY()

	float GroundOffsetZ = 270.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* CollisionComp;

	bool GetRandomPositionInsideBox(AMultiplayerTechTestCharacter * Character, FVector& NewPosition);

	UFUNCTION(NetMulticast, unreliable, WithValidation)
	void Multicast_DrawSpawnZone();

	UFUNCTION(NetMulticast, unreliable, WithValidation)
	void Multicast_DrawDebugPosition(FVector Center, bool Success);

	UFUNCTION(NetMulticast, unreliable, WithValidation)
	void Multicast_DrawDebugCapsule(FVector Center, float Radius, float HalfHeight, bool Success);
	
public:	
	// Sets default values for this actor's properties
	ASpawnZone();

	UFUNCTION(Server, unreliable, WithValidation)
	void Server_GetRandomSpawnPosition(AMultiplayerTechTestCharacter * Character);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
