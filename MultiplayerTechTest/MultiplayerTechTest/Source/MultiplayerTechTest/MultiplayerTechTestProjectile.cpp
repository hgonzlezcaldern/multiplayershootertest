// Copyright Epic Games, Inc. All Rights Reserved.

#include "MultiplayerTechTestProjectile.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"

AMultiplayerTechTestProjectile::AMultiplayerTechTestProjectile() 
{
	bReplicates = true;

	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AMultiplayerTechTestProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
}

void AMultiplayerTechTestProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (GetLocalRole() >= ROLE_Authority) {
		if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
		{
			GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, FString::Printf(TEXT("[LocalRole:%s]: OnHit! Self: %s Other: %s"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth")), *GetName(), *OtherActor->GetName()));
			
			if (OtherComp->IsSimulatingPhysics()) {
				OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
			}

			// Checks if HitActor is a Character, and apply damage if true		
			if (IMultiplayerCharacterInterface* MultiplayerCharacterInterface = Cast<IMultiplayerCharacterInterface>(OtherActor)) {
				GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, FString::Printf(TEXT("[LocalRole:%s]: Interface"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth"))));
				UGameplayStatics::ApplyDamage(OtherActor, BulletDamage,GetOwner()->GetInstigatorController(),this, nullptr);
			
			} else {
				GEngine->AddOnScreenDebugMessage(1, 2, FColor::Emerald, FString::Printf(TEXT("[LocalRole:%s]: no interface"), *((GetLocalRole() >= ROLE_Authority) ? FString("Auth") : FString("NoAuth"))));
			}

			Destroy();
		}
	}
}