// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "./Spawn/SpawnZone.h"
#include "MultiplayerTechTestGameMode.generated.h"

UCLASS(minimalapi)
class AMultiplayerTechTestGameMode : public AGameModeBase
{
	GENERATED_BODY()
	float TimeToRespawn = 10.f;
	
	ASpawnZone * SpawnZoneRef;

protected:
	virtual void BeginPlay() override;

public:

	AMultiplayerTechTestGameMode();

	UFUNCTION(BlueprintCallable)
	void ActorDied(AActor * DeadActor);

	float GetTimeToRespawn();
};



